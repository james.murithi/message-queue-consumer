
## Installation

```bash
$ npm install
```

### DB Setup

```bash
 npx sequelize db:create
 npx sequelize:db migrate
```

### Example .env
```text
WHITELIST_IPS=127.0.0.1,::1,::ffff:127.0.0.1
ACCEPTED_ORIGINS=http://localhost:8080,http://localhost:8081,https://ccm.secure.force.com,http://localhost:3002,http://localhost:3001,https://ccm-core-development.herokuapp.com,https://ccm-core-test.herokuapp.com,https://ccm-core-stage.herokuapp.com,https://ccm-core.herokuapp.com,https://csa-ccm.cs17.force.com,http://localhost:3003
NODE_ENV=development
PORT=3010
DEBUG_MODE=true
DATABASE_USE_SSL=false
DATABASE_URL=postgres://postgres:@d3v2021DBx@127.0.0.1:5440/message_queue_consumer
MESSAGE_QUEUE_RETRY_ATTEMPTS=3
MESSAGE_QUEUE_TIME_TO_RETRY=30
MESSAGE_QUEUE_ENABLED=true
MESSAGE_QUEUE_BATCH_SIZE=1
MESSAGE_QUEUE_PROCESSING_TIMEOUT=10
MESSAGE_QUEUE_BATCH_DELAY=300
CLOUDAMQP_URL=amqp://admin:password@localhost
```

### Example consumer configuration
```text
{ "url":"amqp://admin:password@localhost",
  "exchange":"membership-service",
  "type":"topic",
  "durable":true, "routingKeys":["Membership.Workflow.*"]
}
```

## Contributing

[Yalc](https://www.npmjs.com/package/yalc) is preferred to test new versions of the message queue library

```
yalc publish --update -- library side to add lib to yalc repo
yalc add -- client side to add package from yalc repo e.g. yalc add @ccm-innovation/message-queue@2.1.0
yalc update -- client side to fetch updates from repo
```


## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
