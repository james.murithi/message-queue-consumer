// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

module.exports = {
  development:
    process.env.DATABASE_USE_SSL && process.env.DATABASE_USE_SSL === 'true'
      ? {
          dialectOptions: {
            ssl: {
              require: false,
              rejectUnauthorized: false,
            },
          },
          pool: {
            max: 50,
            min: 0,
            acquire: 30000,
            idle: 10000,
          },
          ssl: false,
          use_env_variable: 'DATABASE_URL',
          seederStorage: 'sequelize',
          seederStorageTableName: 'SequelizeSeeders',
        }
      : {
          pool: {
            max: 50,
            min: 0,
            acquire: 30000,
            idle: 10000,
          },
          use_env_variable: 'DATABASE_URL',
          seederStorage: 'sequelize',
          seederStorageTableName: 'SequelizeSeeders',
        },
};
