import {
  AmqpSequelizeConsumerRepository,
  InMemoryMessageQueueTransactionWorkerRepository,
  Logger,
  MessageQueueService,
  SequelizeMessageQueueTransactionRepository,
  MessageQueueTransactionEntity,
  AmqpSequelizePublisherRepository,
  AmqpMessageQueueService,
} from '@ccm-innovation/message-queue';
import {
  Consumer,
  InboxTransaction,
  Publisher,
  OutboxTransaction,
} from '../db/models';
import { MessageQueueInboxUnrecoverableProcessingError } from '@ccm-innovation/message-queue/dist/src/message-queue/inbox';

export class MessageQueueModule {
  constructor(
    private readonly logger: Logger,
    public readonly service: MessageQueueService,
  ) {}

  public async start(): Promise<void> {
    this.handleGracefulShutdown();
    await this.service.start();

    messageQueueService.inbox.subscribe(
      ['Membership.Workflow.AccountActivation'],
      async (transaction: MessageQueueTransactionEntity) => {
        try {
          console.log('Processed transaction: ', transaction);
        } catch (e) {
          throw new MessageQueueInboxUnrecoverableProcessingError(e.message);
        }
      },
    );

    messageQueueService.inbox.subscribe(
      ['Membership.Workflow.AccountAddOn', ''],
      async (transaction: MessageQueueTransactionEntity) => {
        try {
          console.log('Processed transaction...: ', transaction);
        } catch (e) {
          throw new MessageQueueInboxUnrecoverableProcessingError(e.message);
        }
      },
    );
  }

  public async stop(): Promise<void> {
    await this.service.stop();
  }

  private async handleGracefulShutdown(): Promise<void> {
    await new Promise((resolve) => process.on('SIGINT', resolve))
      .then(async () => await this.service.stop())
      .then(() => process.exit(0))
      .catch(() => process.exit(1));

    await new Promise((resolve) => process.on('SIGTERM', resolve))
      .then(async () => await this.service.stop())
      .then(() => process.exit(0))
      .catch(() => process.exit(1));
    process.on('exit', () => {
      console.log('The application has stopped.');
    });
  }
}

const consumerRepository = new AmqpSequelizeConsumerRepository(Consumer);
const publisherRepository = new AmqpSequelizePublisherRepository(Publisher);
const options = {
  worker: {
    batchSize: +process.env.MESSAGE_QUEUE_BATCH_SIZE,
    batchDelay: +process.env.MESSAGE_QUEUE_BATCH_DELAY,
    processingTimeout: +process.env.MESSAGE_QUEUE_PROCESSING_TIMEOUT,
  },
};

const messageQueueService = new AmqpMessageQueueService(console)
  .configureOutbox(
    options,
    publisherRepository,
    new SequelizeMessageQueueTransactionRepository(OutboxTransaction),
    new InMemoryMessageQueueTransactionWorkerRepository(),
  )
  .configureInbox(
    options,
    consumerRepository,
    new SequelizeMessageQueueTransactionRepository(InboxTransaction),
    new InMemoryMessageQueueTransactionWorkerRepository(),
  );

const messageQueueModule = new MessageQueueModule(console, messageQueueService);

export default messageQueueModule;
