import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import messageQueueModule from './message-queue/message-queue.module';
import * as dotenv from 'dotenv';
import { initializeDb } from './db';

dotenv.config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(process.env.PORT);
  await initializeDb();
  await messageQueueModule.start();
}

const _listeners = new Map<string, (message: any) => unknown>();

export const stringMatchWithWildcard = (
  str: string,
  search: string,
  options?: { caseSensitive?: boolean; wildcardSymbol?: string },
): boolean => {
  const { caseSensitive, wildcardSymbol } = {
    caseSensitive: true,
    wildcardSymbol: '*',
    ...options,
  };
  const searchRegex = new RegExp(
    search
      .split(wildcardSymbol)
      .map((s) => s.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'))
      .join('.*'),
    caseSensitive ? '' : 'i',
  );
  return searchRegex.test(str);
};

export const stringMatchAnyWithWildcard = (
  str: string,
  search: string[],
  options: { caseSensitive?: boolean; wildcardSymbol?: string } = {
    caseSensitive: true,
    wildcardSymbol: '*',
  },
): boolean => {
  return search.some((s) => stringMatchWithWildcard(str, s, options));
};

function subscribe() {
  _listeners.set('Membership.Activate', (msg: any) => {
    console.log(`Activated`, msg);
  });

  _listeners.set('Membership.Withdraw', (msg: any) => {
    console.log(`Withdraw`, msg);
  });

  _listeners.set('Membership.*', (msg: any) => {
    console.log(`Activated or Withdrew.. :shrug: `, msg);
  });
}

function send(event: string, message: any) {
  // find all listeners matching event
  const matchingListeners = [..._listeners.keys()].filter((key: string) =>
    stringMatchWithWildcard(key, event, {
      caseSensitive: true,
      wildcardSymbol: '*',
    }),
  );

  console.log(matchingListeners, ' for message', message);
  for (const key of matchingListeners) {
    console.log('Running fn for event', key);
    const cb = _listeners.get(key);
    cb(message);
  }
}

// subscribe();
// send('Membership.Activate', { payload: { event: 'Add on' } });
bootstrap();
