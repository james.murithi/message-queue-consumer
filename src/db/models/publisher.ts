import { Table } from 'sequelize-typescript';

import { AmqpSequelizeMessageQueuePublisher } from '@ccm-innovation/message-queue';

@Table({
  modelName: 'Publisher',
  tableName: 'publishers',
  underscored: true,
})
export class Publisher extends AmqpSequelizeMessageQueuePublisher {}
