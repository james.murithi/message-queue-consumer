import { Table } from "sequelize-typescript";

import { AmqpSequelizeMessageQueueConsumer } from '@ccm-innovation/message-queue';

@Table({
  modelName: 'Consumer',
  tableName: 'consumers',
  underscored: true,
})
export class Consumer extends AmqpSequelizeMessageQueueConsumer {
}
