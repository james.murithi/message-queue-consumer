import { Table } from 'sequelize-typescript';
import { SequelizeMessageQueueOutboxTransactionModel } from '@ccm-innovation/message-queue';

@Table({
  modelName: 'OutboxTransaction',
  tableName: 'outbox_transactions',
  underscored: true,
})
export class OutboxTransaction extends SequelizeMessageQueueOutboxTransactionModel {}
