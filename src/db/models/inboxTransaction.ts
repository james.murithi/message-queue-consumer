import { Table } from "sequelize-typescript";
import { SequelizeMessageQueueInboxTransactionModel } from "@ccm-innovation/message-queue";

@Table({
  modelName: 'InboxTransaction',
  tableName: 'inbox_transactions',
  underscored: true,
})
export class InboxTransaction extends SequelizeMessageQueueInboxTransactionModel {
}
