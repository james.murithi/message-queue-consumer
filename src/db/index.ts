import { Sequelize } from 'sequelize-typescript';
import { config } from './config';
import {
  Consumer,
  InboxTransaction,
  OutboxTransaction,
  Publisher,
} from './models';
import * as dotenv from 'dotenv';
dotenv.config();

export let MessageQueueSequelize: Sequelize | any = {};

export const initializeDb = async () => {
  try {
    MessageQueueSequelize = new Sequelize(
      process.env.DATABASE_URL,
      config[process.env.NODE_ENV],
    );

    /* prettier-ignore */
    MessageQueueSequelize.addModels([
      Consumer, InboxTransaction, Publisher, OutboxTransaction
    ]);
  } catch (err: any) {
    if (err.name === 'Message Queue SequelizeConnectionError') {
      console.error('Database connection failed:', err.message);
    } else {
      console.error(err);
    }
  }
};
