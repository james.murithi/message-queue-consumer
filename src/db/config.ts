import * as dotenv from 'dotenv';
dotenv.config();

export const getConfiguration = (env = 'development') => {
  return {
    use_env_variable: 'DATABASE_URL',
    seederStorage: 'sequelize',
    seederStorageTableName: 'SequelizeSeeders',
    logging: false,
  };
};

export const config: any = {
  development: getConfiguration('development'),
  test: getConfiguration('test'),
  production: getConfiguration('production'),
};
